/**
 */
package provlang.ecore.provlang;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>prov Base</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link provlang.ecore.provlang.provBase#getActivities <em>Activities</em>}</li>
 *   <li>{@link provlang.ecore.provlang.provBase#getAgents <em>Agents</em>}</li>
 *   <li>{@link provlang.ecore.provlang.provBase#getEntities <em>Entities</em>}</li>
 * </ul>
 *
 * @see provlang.ecore.provlang.ProvlangPackage#getprovBase()
 * @model
 * @generated
 */
public interface provBase extends EObject {
	/**
	 * Returns the value of the '<em><b>Activities</b></em>' containment reference list.
	 * The list contents are of type {@link provlang.ecore.provlang.Activity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activities</em>' containment reference list.
	 * @see provlang.ecore.provlang.ProvlangPackage#getprovBase_Activities()
	 * @model containment="true"
	 * @generated
	 */
	EList<Activity> getActivities();

	/**
	 * Returns the value of the '<em><b>Agents</b></em>' containment reference list.
	 * The list contents are of type {@link provlang.ecore.provlang.Agent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Agents</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agents</em>' containment reference list.
	 * @see provlang.ecore.provlang.ProvlangPackage#getprovBase_Agents()
	 * @model containment="true"
	 * @generated
	 */
	EList<Agent> getAgents();

	/**
	 * Returns the value of the '<em><b>Entities</b></em>' containment reference list.
	 * The list contents are of type {@link provlang.ecore.provlang.Entity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entities</em>' containment reference list.
	 * @see provlang.ecore.provlang.ProvlangPackage#getprovBase_Entities()
	 * @model containment="true"
	 * @generated
	 */
	EList<Entity> getEntities();

} // provBase
