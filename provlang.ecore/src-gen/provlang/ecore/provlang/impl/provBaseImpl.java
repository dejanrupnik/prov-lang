/**
 */
package provlang.ecore.provlang.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import provlang.ecore.provlang.Activity;
import provlang.ecore.provlang.Agent;
import provlang.ecore.provlang.Entity;
import provlang.ecore.provlang.ProvlangPackage;
import provlang.ecore.provlang.provBase;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>prov Base</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link provlang.ecore.provlang.impl.provBaseImpl#getActivities <em>Activities</em>}</li>
 *   <li>{@link provlang.ecore.provlang.impl.provBaseImpl#getAgents <em>Agents</em>}</li>
 *   <li>{@link provlang.ecore.provlang.impl.provBaseImpl#getEntities <em>Entities</em>}</li>
 * </ul>
 *
 * @generated
 */
public class provBaseImpl extends MinimalEObjectImpl.Container implements provBase {
	/**
	 * The cached value of the '{@link #getActivities() <em>Activities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivities()
	 * @generated
	 * @ordered
	 */
	protected EList<Activity> activities;

	/**
	 * The cached value of the '{@link #getAgents() <em>Agents</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgents()
	 * @generated
	 * @ordered
	 */
	protected EList<Agent> agents;

	/**
	 * The cached value of the '{@link #getEntities() <em>Entities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntities()
	 * @generated
	 * @ordered
	 */
	protected EList<Entity> entities;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected provBaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProvlangPackage.Literals.PROV_BASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Activity> getActivities() {
		if (activities == null) {
			activities = new EObjectContainmentEList<Activity>(Activity.class, this,
					ProvlangPackage.PROV_BASE__ACTIVITIES);
		}
		return activities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Agent> getAgents() {
		if (agents == null) {
			agents = new EObjectContainmentEList<Agent>(Agent.class, this, ProvlangPackage.PROV_BASE__AGENTS);
		}
		return agents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Entity> getEntities() {
		if (entities == null) {
			entities = new EObjectContainmentEList<Entity>(Entity.class, this, ProvlangPackage.PROV_BASE__ENTITIES);
		}
		return entities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ProvlangPackage.PROV_BASE__ACTIVITIES:
			return ((InternalEList<?>) getActivities()).basicRemove(otherEnd, msgs);
		case ProvlangPackage.PROV_BASE__AGENTS:
			return ((InternalEList<?>) getAgents()).basicRemove(otherEnd, msgs);
		case ProvlangPackage.PROV_BASE__ENTITIES:
			return ((InternalEList<?>) getEntities()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ProvlangPackage.PROV_BASE__ACTIVITIES:
			return getActivities();
		case ProvlangPackage.PROV_BASE__AGENTS:
			return getAgents();
		case ProvlangPackage.PROV_BASE__ENTITIES:
			return getEntities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ProvlangPackage.PROV_BASE__ACTIVITIES:
			getActivities().clear();
			getActivities().addAll((Collection<? extends Activity>) newValue);
			return;
		case ProvlangPackage.PROV_BASE__AGENTS:
			getAgents().clear();
			getAgents().addAll((Collection<? extends Agent>) newValue);
			return;
		case ProvlangPackage.PROV_BASE__ENTITIES:
			getEntities().clear();
			getEntities().addAll((Collection<? extends Entity>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ProvlangPackage.PROV_BASE__ACTIVITIES:
			getActivities().clear();
			return;
		case ProvlangPackage.PROV_BASE__AGENTS:
			getAgents().clear();
			return;
		case ProvlangPackage.PROV_BASE__ENTITIES:
			getEntities().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ProvlangPackage.PROV_BASE__ACTIVITIES:
			return activities != null && !activities.isEmpty();
		case ProvlangPackage.PROV_BASE__AGENTS:
			return agents != null && !agents.isEmpty();
		case ProvlangPackage.PROV_BASE__ENTITIES:
			return entities != null && !entities.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //provBaseImpl
